﻿using EmergencyX.PluginBase.Interface;
using EmergencyX.PluginBase.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmergencyX.PluginBase.HelpPlugin
{
	public class Help : ICommand
	{
		public string Description
		{
			get
			{
				return "This plugin gives an overview over all currently loaded plugins.";
			}
		}

		public string Name
		{
			get
			{
				return "Help";
			}
		}

		public int Execute(string eventName)
		{
			// ToDo: Make it so, that the help plugin is able to give instructions on how to create a plugin
			Console.WriteLine("Loaded plugins are:");
			foreach (ICommand command in PluginLoadContext.Plugins)
			{
				Console.WriteLine($"{command.Name}: {command.Description}");
			}

			return 0;
		}
	}
}
